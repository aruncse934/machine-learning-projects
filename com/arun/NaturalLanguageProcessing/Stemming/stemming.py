print("----------------NLTK Word Stemming--------------------")

from nltk.stem import PorterStemmer
stemmer = PorterStemmer()
print(stemmer.stem('working'))


print("------------------Stemming Non-English Words---------------------------")

from nltk.stem import SnowballStemmer
print(SnowballStemmer.languages)


print("------------------------------------")

from nltk.stem import SnowballStemmer
french_stemmer = SnowballStemmer('french')
print(french_stemmer.stem("French word"))


# from nltk.stem import SnowballStemmer
# french_stemmer = SnowballStemmer('french')
# print(french_stemmer.stem("French word"))