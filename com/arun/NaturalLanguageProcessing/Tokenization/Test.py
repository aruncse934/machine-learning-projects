import urllib.request
response = urllib.request.urlopen('https://google.com/')
html = response.read()
print (html)

print("-----------------------")

# import nltk
# nltk.download('punkt')
print("----------Sentence Tokenization Using NLTK -----------------")

from nltk.tokenize import sent_tokenize
mytext = "Hello Adam, How are you? I hope everything is going well. Today is a good day, see you dude."
print(sent_tokenize(mytext))

print("----------Word Tokenization-------------")

from nltk.tokenize import word_tokenize
mytext = "Hello Mr. Adam, how are you? I hope everything is going well. Today is a good day, see you dude."
mytext1 = "बैंकों में धोखाधड़ी के सूचित मामले चालू वित्त वर्ष की पहली छमाही के अंत तक 1.13 लाख करोड़ रुपये पर पहुंच गए। इसमें बैंकों को कुछ मामलों का पता देरी से लगा है। भारतीय रिजर्व बैंक (आरबीआई) की एक ताजा रिपोर्ट के अनुसार 1 लाख रुपये और उससे ऊपर के 4,412 धोखाधड़ी मामले में कुल 1.13 लाख करोड़ रुपये राशि जुड़ी है।"
mytext2 = "अतीतानागता भावा ये च वर्तन्ति साम्प्रतम् । तान् कालनिर्मितान् बुध्वा न संज्ञां हातुमर्हसि ॥"
print(word_tokenize(mytext))
print(word_tokenize(mytext1))
print(word_tokenize(mytext2))

print("---------------------Frequency Distribution-------------------")
from nltk.probability import FreqDist
fdist = FreqDist(mytext)
print(fdist)

# Frequency Distribution Plot
import matplotlib.pyplot as plt
fdist.plot(96,cumulative=False)
plt.show()



print("--------------Tokenize Non-English Languages Text----------------")

from nltk.tokenize import sent_tokenize
mytext = "Bonjour M. Adam, comment allez-vous? J'espère que tout va bien. Aujourd'hui est un bon jour."
print(sent_tokenize(mytext,"french"))

print("-------------------Get Synonyms From WordNet-------------------")

# import nltk
# nltk.download('wordnet')

from nltk.corpus import wordnet
syn = wordnet.synsets("pain")
print(syn[0].definition())
print(syn[0].examples())

print("---")

from nltk.corpus import wordnet
syn = wordnet.synsets("NLP")
print(syn[0].definition())
syn = wordnet.synsets("Python")
print(syn[0].definition())

print("------------------")


from nltk.corpus import wordnet
synonyms = []
for syn in wordnet.synsets('computer'):
    for lemma in syn.lemmas():
        synonyms.append(lemma.name())
print(synonyms)


print("----------Get Antonyms From WordNet------------")

from nltk.corpus import wordnet
antonyms = []
for syn in wordnet.synsets("small"):
    for l in syn.lemmas():
        if l.antonyms():
            antonyms.append(l.antonyms()[0].name())
print(antonyms)


