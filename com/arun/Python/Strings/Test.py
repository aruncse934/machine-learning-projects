#String::- A string is a series of characters, surrounded by single or double quotes.
#Concatenation (combining strings)

first_name ="Arun"
middle_name ="Kumar"
last_name ="Gupta"
full_name =first_name +' '+ middle_name + ' '+ last_name
print(full_name)

first_name ='Arun'
middle_name ='Kumar'
last_name ='Gupta'
full_name =first_name +' '+ middle_name + ' '+ last_name
print(full_name)